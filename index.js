// Include express module
const express = require("express");

// package allows to create schema to model our data structure
const mongoose = require("mongoose");

// setup express server
const app = express();

// list the port where the srver will listen 
const port = 3001;

// MongoDB connection
/*
	Syntax:
	    mongoose.connect("<MongoDB Atlas Connection String>", 
	    {useNewUrlParser: true, useUnifiedTopology: true});
*/
mongoose.connect("mongodb+srv://admin:admin@cluster0.2cnfh.mongodb.net/b183_to-do?retryWrites=true&w=majority",
{useNewUrlParser: true, useUnifiedTopology: true});

// set notification for connection succes or error
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))

// if the connection is success a console message will show
db.once("open", () => console.log("We're connected to the cloud database."));

// mongoose schema ==============================================================================
// determine the structure of the document to be written in the database
// blueprints to our data
const taskSchema = new mongoose.Schema({

	// string is a shorthand for name:{type: String}
	name: String,
	// status task (Complete, pending, Incomplete)
	status:{
		type: String,
		// default values are the predefined values for a field if we don't put any value.
		default: "Pending"
	}
})
// Mongooese Model ================================================================================
/*
		Syntax:
		   const modelName = mongoose.model("collectionName", mongooseSchema);
*/
// model must be in singular form and capitalize the first letter
// Using mongoose the package was programed well enough that it automatically 
// converts the singular form of the model name into a plural form when creating a collection 
	const Task = mongoose.model("Task", taskSchema);

// middleware==============================================
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for creating a task====================================================
// Business Logic
// 
// 	1. Add a functionality to check if there are duplicate tasks
// 		- If the task already exists in the database, we return an error
// 		- If the task doesn't exist in the database, we add it in the database
// 	2. The task data will be coming from the request's body
// 	3. Create a new Task object with a "name" field/property
// 	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
app.post("/tasks", (req,res) => {
	// Call bakc function in mongoose are programmed this way 
	// first parameter store the error
	// second parameter return the result 
	// req.body.name=eat
	// Task.findOne({name:eat})
	Task.findOne({name:req.body.name}, (err, result) =>{
			console.log(result);

			// if a document was found and the document's name matches the info sent 
			// by the client / postman 
			if(result != null && req.body.name == result.name){
				// return a message to the client or postmn 
					return res.send("Duplicate task found!");
			}
			// if no document was found or no duplicate 
			else{
				// Create a new task and save to database 
				let newTask = new Task({
					name: req.body.name
				});

				// the "save:" method will store the information to the database
				// since the "newTask" was created/instantiated from the task model that contains
				// the mongoose Schema

				newTask.save((saveErr, saveTask) =>{
					// if there are errors in saving it will be displayed in the console.
					if(saveErr){
						return console.error(saveErr);
					}
					// if no error found while creating
					else{
						return res.status(201).send("New task created");
					}
				})
			}
	})
})

app.get("/tasks", (req, res) =>{
    Task.find({}, (err, result) => {
        if(err){
            return console.log(err);
        }
        else{
            return res.status(200).send(result)
        }
    })
})

// Activity
/*
	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.
*/

// listen to port
app.listen(port, () => console.log(`Server running at port ${port}`));




















































