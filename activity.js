// Include express module
const express = require("express");

// package allows to create schema to model our data structure
const mongoose = require("mongoose");

// setup express server
const app = express();

// list the port where the srver will listen 
const port = 3001;

// MongoDB connection
/*
	Syntax:
	    mongoose.connect("<MongoDB Atlas Connection String>", 
	    {useNewUrlParser: true, useUnifiedTopology: true});
*/
mongoose.connect("mongodb+srv://admin:admin@cluster0.2cnfh.mongodb.net/b183_to-do?retryWrites=true&w=majority",
{useNewUrlParser: true, useUnifiedTopology: true});

// set notification for connection succes or error
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))

// if the connection is success a console message will show
db.once("open", () => console.log("We're connected to the cloud database."));

// mongoose schema ==============================================================================
// determine the structure of the document to be written in the database
// blueprints to our data
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// Mongooese Model ================================================================================
// Model uses Schemas and they act as the middleman from the server (JS code) to our database.
/*
	Syntax:
	const modelName = mongoose.model("collectionName", mongooseSchema);
*/
// Model must be in singular form and capitalize the first letter.

const User = mongoose.model("User", userSchema);

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for creating a user
// Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.

*/
app.post("/signup", (req, res) => {
	if (req.body.username == "" && req.body.password == ""){
		return res.send("Username and Password are required.");
	}

// Call bakc function in mongoose are programmed this way 
	// first parameter store the error
	// second parameter return the result 
	// req.body.name=eat
	// Task.findOne({name:eat})
	User.findOne({username: req.body.username}, (err, result) => {
		if (result != null && req.body.username == result.username){
			// Retrn a message to the client
			return res.send("Duplicate user found!");
		}
		// If no document was found or no duplicate.
		else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveResult) => {
				// If there are errors in saving it will be displayed in the console.
				if (saveErr) {
					return console.error(saveErr)
				}
				// If no error found while creating the document it will be save in the database
				else {
					return res.status(200).send("New user created.");
				}
			});
		}
	})
})
// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`))





























